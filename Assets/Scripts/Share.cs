﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Share : MonoBehaviour
{
    public void ShareSS(){
        StartCoroutine(CaptureScreenshot(0,Application.temporaryCachePath+"/a.png"));
    }

    public static IEnumerator CaptureScreenshot(float waitDuration,string path){
        yield return new WaitForSeconds(waitDuration);
        yield return new WaitForEndOfFrame();
	    Texture2D ss = new Texture2D( Screen.width, Screen.height, TextureFormat.RGB24, false );
        if( !SaveScreenShot(path,ss) )
            path = null;

        new NativeShare().AddFile(path).Share();
    }
    private static bool SaveScreenShot(string path,Texture2D ssData){
        try{
            ssData.ReadPixels(new Rect(0,0,Screen.width,Screen.height),0,0);
            ssData.Apply();
            File.WriteAllBytes( path, ssData.EncodeToPNG() );
            UnityEngine.Object.Destroy(ssData);
            return true;
        }catch(System.Exception e){
            Debug.Log(e);
            return false;
        }
    }
}
