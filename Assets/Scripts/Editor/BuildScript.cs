﻿using UnityEditor;

public class BuildScript
{
    static void PerformBuild(){
        string[] defaultScenes = {"Assets/Scenes/MainScene.Unity"};
        BuildPipeline.BuildPlayer(defaultScenes, "./build/myapp.apk",BuildTarget.Android , BuildOptions.None);
    }
}
